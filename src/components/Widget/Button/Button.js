import React from "react";
import "./Button.css";

const TYPE = ["btn--filled", "btn--outlined", "btn--link"];
const SIZE = ["btn--small", "btn--medium", "btn--large"];
const COLOR = ["yellow", "black"];

const Button = ({ btnType, btnSize, btnColor, children, onClick, type }) => {
  const checkButtonType = TYPE.includes(btnType) ? btnType : TYPE[0];
  const checkButtonSize = SIZE.includes(btnSize) ? btnSize : SIZE[0];
  const checkButtonColor = COLOR.includes(btnColor) ? btnColor : null;

  console.log(checkButtonColor, btnColor);
  console.log(checkButtonSize, btnSize);
  console.log(checkButtonType, btnType);
  return (
    <button
      type={type}
      className={`btn ${checkButtonType} ${checkButtonSize} ${checkButtonColor}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

export default Button;
