import React from 'react'
import './Section.css'

const FORMAT = ["text-left", "text-right", "card"]
const HEIGHT = ["full", "partial", "half"]
const Section = ({height, format, header, tagLine, desc, button, img}) => {
  const checkFormat = FORMAT.includes(format)? format : "text-left";
  const checkHeight = HEIGHT.includes(height)? height : null;

  return (
    <>
      <section className={`${checkHeight} ${checkFormat}`}>
        <div className="hero">
          <h4><b>{header}</b></h4>
          <p className='tag-line'>{tagLine}</p>
          <p className='description'>{desc}</p>
          {button}
        </div>
        <div className="image">
          <img src={img} alt={img}/>
        </div>
      </section>
    </>
  )
}

export default Section
