import React, { useState, useEffect } from "react";
import "./Navbar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { faX } from "@fortawesome/free-solid-svg-icons";

const Navbar = () => {
  const [toogleMenu, setToogelMenu] = useState(false);
  // const [width, setWidth] = useState(0);

  const toogleHandler = ()=>{
    setToogelMenu(!toogleMenu);
  }

  // useEffect(() => {
  //   setWidth(window.innerWidth);
  // }, []);

  console.log(toogleMenu);



  return (
    <nav>
      <div className="logo">Dhan</div>
      <div className="toogle-menu">
        {toogleMenu ? <FontAwesomeIcon icon={faX} onClick={toogleHandler}/>:<FontAwesomeIcon icon={faBars} onClick={toogleHandler}/>}
        
      </div>
      <div className={`${toogleMenu?"nav-menu-toogle-actived": "nav-menu"}`}>
        <ul className="nav-links">
          <li className="nav-link">Home</li>
          <li className="nav-link">About</li>
          <li className="nav-link">Gallery</li>
          <li className="nav-link">Project</li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
