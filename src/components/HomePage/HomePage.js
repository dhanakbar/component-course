import React from 'react'
import Button from '../Widget/Button/Button'
import Navbar from '../Widget/Navbar/Navbar'
import Section from '../Widget/Section/Section'

const HomePage = () => {
  return (
    <>
      <Navbar/>
      <Section height="full" format="text-left" header="Let Me Develop Your Design" img="https://www.pngmart.com/files/21/3D-Character-PNG-HD-Isolated.png" tagLine="Frontend and Backend Website" desc="Quis eiusmod consequat exercitation deserunt incididunt excepteur nulla ad aute sunt voluptate. Sit dolor pariatur amet ad dolore Lorem nisi in qui duis enim cupidatat ad non. Cupidatat magna anim qui consectetur mollit duis elit. Duis ad aliqua proident ea id." button={<Button btnColor="primary" btnType="btn--filled" btnSize="btn--medium" children="Get Touch with"/>}/>
    </>
  )
}

export default HomePage
